# python

Cours de Programmation répartie

Lien des sujets de tp : https://fc.isima.fr/~lcrombez/pedagogie.html

# TP réalisé en cours et à domicile

Composé de 6 tp:

1. Découverte du python:
   * [x]  Exercice 1: Tri Dichotomique
   * [x] Exercice 2: Recherche Dichotomique
   * [x] Exercice 3: Génération de Diamant

2. Gestion des threads
   * [x] Exercice 1: Téléchargement d'images
   * [x] Exercice 2: Division d'image

3. Manipulation de graphe
   * [x] Création du graphe et ses fonctions vitales
   * [x] Gestion de l'estampillage et des messages

4. Gestion de server
   * [x] Découverte du typage et des entrées clavier
   * [x] Manipulation de liste numpy
   * [x] Création d'un server/client

5. Réseau de confiance
   * [x] Créer un dictionnaire à partir de données
   * [x] Gestion dinamique des membres autorisés en JSON
   * [x] Diffusion de message à un groupe de membre

6. Amélioration du tp "5. Réseau de confiance"
   * [x] Mise à jour collection de fichiers
   * [x] Diffusion de mise à jour de fichiers
   * [x] Strategie de download chez le premier membre

7. Echange de fichier
   * [ ] Dépôt de fichier
   * [ ] Requête download
   * [ ] Gestion du menu
   * [ ] Stratégie téléchargement du plus rare