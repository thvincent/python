from tp4_functions import *

import sys
import numpy as np
import xmlrpc.client

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Veuillez entrer un entier')
	else:
		try:
			n = int(sys.argv[1])
			mfunc(sys.argv[1])
			pass
		except ValueError as e:
			print('Veuillez entrer un entier')
			exit(0)

m1 = np.random.rand(n)
m1 *= 100
m1 = m1.astype(int)
m1 = list(m1)
for i in range (0, len(m1)):
	m1[i] = int(m1[i])
	print(m1[i])

liste1 = is_prime(m1)
print(liste1)

def outside_even(n):
	with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:
		return proxy.is_prime(listeA)

def outside_even2(n):
	with xmlrpc.client.ServerProxy("http://localhost:8001/") as proxy:
		return proxy.is_prime(listeB)


listeA = m1[:len(m1)//2]
listeB = m1[len(m1)//2:]

repA = outside_even(listeA)
repB = outside_even2(listeB)

print(repA + repB)
#n = 'a'
#type('a') == int
#True