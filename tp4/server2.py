from tp4_functions import *
from xmlrpc.server import SimpleXMLRPCServer

server = SimpleXMLRPCServer(("localhost", 8001))
server.register_function(is_prime, "is_prime")
server.serve_forever()