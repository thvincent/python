def is_prime(ml):
	liste = []
	for i in range(0, len(ml)):
		if is_nombrePremier(ml[i]) == True:
			liste.append(ml[i])
	return liste

def is_nombrePremier(nb):
	if nb <= 1:
		return False
	for i in range(2,nb):
		if (nb % i) == 0:
			return False
	return True

def mfunc(n):
	print('Vous avez entrez', n)