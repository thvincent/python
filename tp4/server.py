from tp4_functions import *
from xmlrpc.server import SimpleXMLRPCServer

server = SimpleXMLRPCServer(("localhost", 8000))
server.register_function(is_prime, "is_prime")
server.serve_forever()