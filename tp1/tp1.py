#!/usr/bin/env python

def dicho_sort(input_list):
	if len(input_list) == 0:
		return
	elif len(input_list) <= 1:
		return input_list

	tableau1 = input_list[:len(input_list)//2]
	tableau2 = input_list[len(input_list)//2:]

	tableau1 = dicho_sort(tableau1)
	tableau2 = dicho_sort(tableau2)

	return fusion(tableau1, tableau2)


def fusion(tableau1, tableau2):
	tableau=[]
	i1=0
	i2=0

	while(i1 < len(tableau1) and i2 < len(tableau2)):
		if tableau1[i1] < tableau2[i2]:
			tableau.append(tableau1[i1])
			i1+=1
		else:
			tableau.append(tableau2[i2])
			i2+=1

	if i1 < len(tableau1):
		while i1 < len(tableau1):
			tableau.append(tableau1[i1])
			i1+=1
	else:
		while i2 < len(tableau2):
			tableau.append(tableau2[i2])
			i2+=1

	return tableau


def dicho_find(input_list, find_me):
	if len(input_list) == 1:
		if input_list[0] != find_me:
			return None

	milieu=(len(input_list))//2
	
	if input_list[milieu] == find_me:
		return milieu


	if input_list[milieu] < find_me:
		if dicho_find(input_list[milieu:], find_me) == None:
			return None
		return dicho_find(input_list[milieu:], find_me) + milieu
	else:
		if dicho_find(input_list[:milieu], find_me) == None:
			return None
		return dicho_find(input_list[:milieu], find_me)


def get_diamond(diamond_size):
	if diamond_size%2 == 0:
		return None
	i=1
	message=""
	while i<=diamond_size:
		message+=(" " * ((diamond_size - i)//2))
		message+=("*" * i)
		message+=("\n")
		i+=2
	i-=4
	while i>0:
		message+=(" " * ((diamond_size - i)//2))
		message+=("*" * i)
		message+=("\n")
		i-=2
	return message


print("--------- Exercice 1 ---------\n\n")

monTableau=[9,5,7,3,1,6]
print("Tableau initial : " + str(monTableau))
monTableau = dicho_sort(monTableau)
print("Tableau trié : " + str(monTableau))


print("\n\n--------- Exercice 2 ---------\n\n")

print("Le chiffre 7 est à l'index : " + str(dicho_find(monTableau, 7)))
print("Le chiffre 10 est à l'index : " + str(dicho_find(monTableau, 10)))


print("\n\n--------- Exercice 3 ---------\n\n")

print("Diamant de taille 6 : " + str(get_diamond(6)))
print("Diamant de taille 7 :\n" + get_diamond(7))
print("Diamant de taille 9 :\n" + get_diamond(9))
