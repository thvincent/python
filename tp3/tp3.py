import tp3_functions as tp3

edges_list = [('p_0_1', 'p_0_2'),('p_0_2', 'p_0_3')]


directed_graph = tp3.get_graph(edges_list)
tp3.get_lamport_time(directed_graph)
tp3.Estempilles(directed_graph)
tp3.get_cuts(directed_graph)
tp3.dessinGraph(directed_graph)

