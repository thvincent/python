import networkx as nx
import matplotlib.pyplot as plt


def get_graph(edges_list):
	G = nx.DiGraph()
	for i in edges_list:
		n1=i[0].split(',')
		n2=i[1].split(',')
		G.add_nodes_from([n1[0],n2[0]])
		G.add_edge(n1[0],n2[0])
	
	options={
		'node_color':'pink',
		'node_size':5000,
		'edge_color':'pink',
		'font_weight':'bold',
	}
	nx.draw_shell(G, with_labels=True,**options)
	plt.show()


get_node_attributes(G, name)

def get_lamport_time(directed_graph):
	firstNode= []
    nodeCheck = []
    for n in directed_graph.nodes():
        for pred in directed_graph.predecessors(n):
            directed_graph.nodes[n]['estampille']=1
        if 'estampille' not in directed_graph.nodes[n]:           
            directed_graph.nodes[n]['estampille']=1 
            directed_graph.nodes[n]['message']= "Je suis le noeud " + str(1)
            firstNode.append(n)
    for n in directed_graph.nodes():
        for pred in directed_graph.predecessors(n):
            if n not in firstNode :
                if n not in nodeCheck :
                    directed_graph.nodes[n]['estampille']=directed_graph.nodes[pred]['estampille']+1
                    directed_graph.nodes[n]['message']="Je suis le noeud " + str(directed_graph.nodes[pred]['estampille']+1)
                    nodeCheck.append(n)


def get_cuts(directed_graph):
	for node in directed_graph.nodes():
		msg = {}
		fmsg = {}                               
		successor = []                                 
		if next(directed_graph.successors(node),False) is not None :
			for suc in directed_graph.successors(node):
				successor.append(suc)
		msg['nodes']=successor
		if 'estampille' in directed_graph.nodes[node]:
			msg['mes'] = directed_graph.nodes[node]
		else :
			msg['mes'] = ' '
		fmsg[node]=msg
		print(fmsg)


def Estempilles(directed_graph) :
    for n in directed_graph.nodes():                   
        if 'estampille' in directed_graph.nodes[n]:
            print(n," : ",directed_graph.nodes[n])
        if 'message' in directed_graph.nodes[n]:
            print(n," : ",directed_graph.nodes[n])

def dessinGraph(directed_graph):
	nwx.draw(directed_graph,with_labels=True)
	plt.show()






