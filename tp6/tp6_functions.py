import os
from os import listdir
from os.path import isfile, join
import xmlrpc.client
from xmlrpc.server import SimpleXMLRPCServer


def addDict(dic, file, directory):
	if directory == "completeFiles":
		complete = True
	else :
		complete = False
	nom = files.split("_")[0]
	chunks_number = int(files.split("_")[1].split(".")[0])
	if (nom in dic):
		dic[nom]['chunks_number'].append(chunks_number)
	else:
		dic[nom] = { "complete":complete, "chunks_number":[chunks_number] }
	return dic

def parse_files():
	listCompleteFiles = [files for files in listdir("complete") if isfile(join("complete", files))]
	listChunksFiles = [files for files in listdir("chunks") if isfile(join("chunks",files))]
	dictionnaire = {}
	for files in listCompleteFiles:
		dictionnaire = addDict(dictionnaire, files, "completeFiles")
	for files in listChunksFiles:
		dictionnaire = addDict(dictionnaire, files, "chunksFiles")
	return dictionnaire

def update_files(source, dictionnaire):
	newDict = parse_files()
	if dictionnaire == {}:
		return newDict
	else:
		for files in dictionnaire:
			modif = 0
			nom = files.split("_")[0]
			for otherFiles in newDict:
				if(otherFiles==files):
					print("Le fichier existe deja")
					modif=1
			if(modif!=1):
				newDict=addDict(newDict, files, source)
	return newDict

def broadcast():
	server = SimpleXMLRPCServer(("localhost", 8002))
	print("Listening on port 8002...")
	server.register_function(update_files,"update_files")
	server.serve_forever()


def download_strategy(files):
	listCompleteFiles = [files for files in listdir("complete") if isfile(join("complete", files)) ]
	listChunksFiles = [files for files in listdir("chunks") if isfile(join("chunks",files))]
	dico = {}
	for verif in listCompleteFiles:
		if verif.split("_")[0] == files:
			dico[verif.split("_")[1].split(".")[0]]=verif
	for verif in listChunksFiles:
		if verif.split("_")[0] == files:
			dico[verif.split("_")[1].split(".")[0]]=verif
	return dico

