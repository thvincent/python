import json
import os
import xmlrpc.client


#permettra de créer un dictionnaire à partir :
# d’un id (alphanum) ;
#d’un nom ;
#d’une ip ;
#d’un port ;
#du nom de fichier d’une clef public (que vous aurez générée au préalable).
#Cette fonction sérialisera ces données en accord avec les informations suivantes.
def forge_member(id, name, ip, port,key_filename):
	dict= {}
	dict["id"] = id
	dict["name"] = name
	dict["ip"] = ip
	dict["port"] = port
	dict["key_filename"] = key_filename

	with open('trusted_nodes/'+id+'.json', 'w', encoding='utf-8') as f:
		json.dump(dict, f)



#permettre au programme de mettre à jour sa liste de membres 
#(une variable "member_list", une liste de dictionnaires) 
#à partir des fichiers présents dans le répertoire.
def parse_confs(dir_name):
	list_fi = os.listdir(dir_name)
	member_list = []

	for f in list_fi:
		with open('trusted_nodes/'+f, 'r', encoding='utf-8') as data:
			open_data= json.load(data)
			member_list.append(open_data)
		
	print(member_list)
	return member_list


def update_members(member_list):
	list_enr = parse_confs('trusted_nodes/')

	for m in member_list:
		ok=0
		for n in list_enr:
			if(m["id"] == n["id"]):
				print('Membre déjà enregistré')
				ok = 1
		if(ok == 0): 
			forge_member(m["id"], m["name"], m["ip"],m["port"],m["key_filename"])
	return 0


def broadcast():
	members = parse_confs('trusted_nodes/')

	for m in members:
		with xmlrpc.client.ServerProxy("http://localhost:"+m["port"]+"/") as proxy:
			print(m["port"])
			proxy.update_members(members)




def main():
	member_list = []

	forge_member('111','Zeus','localhost','8000','id_rsa_thvincent6.pub')
	forge_member('222','Hades','localhost','8001','id_rsa_thvincent6.pub')
	forge_member('666','Cerbere','localhost','8002','id_rsa_thvincent6.pub')
	member_list = parse_confs('trusted_nodes/')

	dictionnaire = {}
	dictionnaire["id"] = '555'
	dictionnaire["name"] = 'Apollon'
	dictionnaire["ip"] = 'localhost'
	dictionnaire["port"] = '8003'
	dictionnaire["key_filename"] = 'id_rsa_thvincent6.pub'

	diction = {}
	diction["id"] = '999'
	diction["name"] = 'Meduse'
	diction["ip"] = 'localhost'
	diction["port"] = '8004'
	diction["key_filename"] = 'id_rsa_thvincent6.pub'

	#member_list.append(dictionnaire)
	#member_list.append(diction)

	update_members(member_list)

	print(member_list)

	broadcast()




