import tp5 as tp
from xmlrpc.server import SimpleXMLRPCServer




server = SimpleXMLRPCServer(('localhost',8002))
print("Listening on port 8002...")
server.register_function(tp.update_members, "update_members")
server.serve_forever()