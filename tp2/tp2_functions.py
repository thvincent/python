import concurrent.futures as cf
import timeit
import math
import urllib.request
import cv2 as cv

def fast_dl(url, nb_thread): # url = adresse de téléchargement / nb_thread = nombre de thread
	executor = ThreadPoolExecutor(max_workers=nb_thread);
	for i in range(1,20) :
		executor.submit(thread_function(url, i));


def load(url):
	urllib.request.urlretrieve(url, "dog.jpg")

def fast_Laplacian(target_image, nb_thread): 
	ddepth = cv.CV_16S;
	size = 50;
	src = cv.imread(cv.samples.findFile(target_image), cv.IMREAD_COLOR);
	if src is None: 
		print("Erreur lors de l'ouverture de l'image");
		return -1;
	cv.Laplacian(ddepth,ksize=size);
	
	return 0;




